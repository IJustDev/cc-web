import React from 'react';
import './IconCard.css';
import './Card.css';

function IconCard(props) {
  return (
      <div className="card">
        <img src="{props.iconUrl}" alt="Icon"/>
        <h1 className="card-title">{props.title}</h1>
        {props.children}
      </div>
  );
}

export default IconCard;
