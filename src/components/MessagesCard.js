import React, { Component } from 'react';
import Card from '../Card';
import './MessagesCard.css';

class MessagesCard extends Component {

  mockMessages() {
    const elements = [{
      user: 'Sansa22',
      message: 'Entry Problem',
    }, {
      user: 'Joesay11',
      message: 'More informations',
    }, {
      user: 'Joesay11',
      message: 'More informations',
    }, {
      user: 'Joesay11',
      message: 'More informations',
    }];
    const messagesArray = [];
    for (const element of elements) {
      messagesArray.push(<tr><td>{element.user}</td><td>{element.message}</td><td>Reply</td></tr>);
    }
    this.messagesArray = messagesArray;
  }

  render() {
    this.mockMessages();
    return (
      <Card renderLine={true} title="Messages">
        <div className="messages-card">
          <table>
            <thead>
              <th>ID</th>
              <th>Messages</th>
              <th></th>
            </thead>
            <tbody>
              {this.messagesArray}
            </tbody>
          </table>
        </div>
      </Card>
    );
  }
}

export default MessagesCard;
