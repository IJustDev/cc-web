import React from 'react';
import './UpcomingEvents.css';
import StatusBulp from './StatusBulp';

import {numberWithCommas} from '../utils/numbers';

export default function UpcomingEvents({events}) {
  const status = [
    {
      text: 'Sold out',
      color: '#ff8f00'
    },
    {
      text: 'Sold out soon',
      color: '#ffcc41'
    },
    {
      text: 'Free registration',
      color: '#7ed321'
    },
    {
      text: 'Pending',
      color: '#9c80f7'
    },
  ];
  for (const event of events) {
    event.tagColor = status[event.status].color;
    event.tagText = status[event.status].text;
  }
  return (
    <div className="upcoming-events">
      <ul>
        {events.map((event) => {
          return (
            <li>
              <div className="left">
                <div className="status-icon">
                  <StatusBulp color={event.tagColor}/>
                </div>
              </div>
              <div className="right">
                <div className="date-time smaller low-opacity">
                  {event.date} | {event.time}
                </div>
                <div className="title">
                  {event.title}
                </div>
                <div className="tags">
                  <div className="tag" style={{backgroundColor: event.tagColor}}>
                    {event.tagText}
                  </div>
                </div>
              </div>
            </li>
          )
        })}
      </ul>
    </div>
  )
}
