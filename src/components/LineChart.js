import React, { Component } from "react";
import { scaleLinear, scaleTime } from "d3-scale";
import { max, extent } from "d3-array";
import { select } from "d3-selection";
import { axisLeft, axisBottom } from "d3-axis";
import { arc, line, curve, curveBasis } from "d3-shape";

class LineChart extends Component {
  constructor(props) {
    super(props);
    this.createLineChart = this.createLineChart.bind(this);
  }

  componentDidMount() {
    this.createLineChart();
  }

  createLineChart() {
    const size = {
      width: this.props.size[0],
      height: this.props.size[1],
    };
    const width = size.width,
    height = size.height;

    const node = this.node;
    const data = this.props.data;
    const svg = select(this.node)
      .attr('width', width)
      .attr('height', height);

    const yScale = scaleLinear()
      .domain([0, max(data, d => +d.y)])
      .range([height, 0]);
    svg.append("g")
      .call(axisLeft(yScale))

    const xScale = scaleTime()
      .domain(extent(data, d => +d.x))
      .range([0, width])
    svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(axisBottom(xScale));


    const smoothed = this.props.smoothed || false;

    const l = line()
        .x(d => xScale(d.x))
        .y(d => yScale(d.y))

    svg.append("path")
      .datum(data)
      .attr("fill", "none")
      .attr("stroke", "darkblue")
      .attr("stroke-width", this.props.strokeWidth || 1.5)
      .attr("d", smoothed ?
        l.curve(curveBasis) :
        l
      );
  }

  render() {
    return <svg ref={node => this.node = node}>
    </svg>
  }
}

export default LineChart;
