import React from 'react';
import ProgressBar from '../components/ProgressBar';
import {numberWithCommas} from '../utils/numbers';

import './LegendProgressBar.css';

export default function LegendProgressBar(props) {
  return (
    <div className="legend-progressbar">
      <div class="legend">
        <div>{props.text}</div>
        <div>{numberWithCommas(props.value)} ({Math.round(props.value/props.max * 100, 2)}%)</div>
      </div>
      <ProgressBar
        primaryColor={props.primaryColor}
        size={['100%']}
        value={props.value}
        max={props.max}/>
    </div>
  )
}
