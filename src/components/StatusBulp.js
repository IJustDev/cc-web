import React, {Component} from 'react';
import {select} from 'd3-selection';
import {arc, pie} from 'd3-shape';

class CircleChart extends Component {

  constructor(props) {
    super(props);
    this.createStatusBulp = this.createStatusBulp.bind(this);
  }

  componentDidMount() {
    this.createStatusBulp();
  }

  createStatusBulp() {
    const size = {
      width: 24,
      height: 24,
    };
    const node = this.node;
    const data = this.props.data;
    const svg = select(this.node)
      .attr('width', size.width)
      .attr('height', size.height);

    const arcGen = arc()
      .startAngle(0)
      .endAngle(Math.PI * 2)
      .innerRadius(0)
      .outerRadius(size.width/4);

    svg.append('path')
      .attr('fill', this.props.color)
      .attr('d', arcGen)
      .attr('transform', `translate(${size.width/2}, ${size.height/2})`);
  }

  render() {
    return (
      <svg ref={node => this.node = node}></svg>
    );
  }
}

export default CircleChart;

