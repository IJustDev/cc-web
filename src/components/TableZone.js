import React, { Component } from 'react';


import './TableZone.css';


class TableZone extends Component {
    // mockZones() {
    //     const elements = [{
    //         action: 'Click',
    //         value: '4,275',
    //     }, {
    //         action: 'View',
    //         value: '12,773',
    //     }, {
    //         action: 'Sales',
    //         value: '639',
    //     }];
    //     const zoneArray = [];
    //     for (const element of elements) {
    //         zoneArray.push(<tr><td>{element.action}</td><td>{element.value}</td></tr>);
    //     }
    //     this.zoneArray = zoneArray;
    // }

    createTable() {
        const zoneArray = [];
        const data = this.props.data;
        for (const element of data) {
            zoneArray.push(<tr><td>{element.action}</td><td>{element.value}</td></tr>);
        }
        this.zoneArray = zoneArray;
    }
    render() {
        // this.mockZones();
        this.createTable();
        return (
            <div className="table-zone">
                <table>
                    <tbody>
                        {this.zoneArray}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default TableZone;