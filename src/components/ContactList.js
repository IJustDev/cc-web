import React from 'react';

import List from './List';
import ListItem from './ListItem';

import './ContactList.css';
import avatar from '../static/img/icons/avatar.svg';

function ContactList(props) {
  const contacts = [
    {username: 'IJustDev', agentID: 420, score: 69},
    {username: 'OtherUser', agentID: 69, score: 75},
  ]
  for (const contact of contacts) {
    contact.scoreColor = contact.score >= 75 ? '#7ed321' : '#3b7cff';
  }
  return (
    <div className="contact-list">
      <List>
        {contacts.map((contact) => {
          return(
            <ListItem>
              <div className="generic-information">
                <img
                  width="64" height="64"
                  className="agent-img" src={avatar}/>
                <div className="informations">
                  <h1>{contact.username}</h1>
                  <h3>Agent-ID: {contact.agentID}</h3>
                  <div className="score" style={{color: contact.scoreColor}}>
                    {contact.score} / 100
                  </div>
                </div>
              </div>
            </ListItem>
          )
        })}
      </List>
    </div>
  )
}

export default ContactList;
