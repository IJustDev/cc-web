import React, { Component } from "react";
import { scaleLinear } from "d3-scale";
import { max } from "d3-array";
import { select } from "d3-selection";
import { axisLeft } from "d3-axis";
import { arc } from "d3-shape";

import './RadialProgressChart.css';

// https://codepen.io/dabrorius/pen/dWmOgB

class DoubleCircleChart extends Component {

  constructor(props){
    super(props)
    this.createProgressChart = this.createProgressChart.bind(this);
  }

  componentDidMount() {
    this.createProgressChart().update(this.props.value);
  }

  componentDidUpdate() {
    this.createProgressChart().update(this.props.value);
  }

  createProgressChart() {
    const node = this.node;
    const size = {
      width: this.props.size[0],
      height: this.props.size[1],
    };
    const svg = select(this.node)
      .attr('width', size.width)
      .attr('height', size.height);
    const outerRadius = Math.min(size.width, size.height) * 0.25;
    const thickness = 10;
    let value = 0;

    const mainArc = arc()
      .startAngle(0)
      .endAngle(Math.PI * 2)
      .innerRadius(outerRadius-thickness)
      .outerRadius(outerRadius)

    const outerArc = arc()
      .startAngle(0)
      .endAngle(Math.PI * 2)
      .innerRadius(outerRadius-thickness + 30)
      .outerRadius(outerRadius + 30);

    svg.append("path")
      .attr('class', 'progress-bar-bg')
      .attr('transform', `translate(${size.width/2},${size.height/2})`)
      .attr('d', mainArc())

    svg.append("path")
      .attr('class', 'progress-bar-bg')
      .attr('transform', `translate(${size.width/2},${size.height/2})`)
      .attr('d', outerArc())

    const mainArcPath = svg.append("path")
      .attr('class', 'progress-bar')
      .attr('transform', `translate(${size.width/2},${size.height/2})`);

    const outerArcPath = svg.append("path")
      .attr('class', 'progress-bar')
      .attr('transform', `translate(${size.width/2},${size.height/2})`);


    return {
      update: function(progressPercent) {
        const startValue = value
        const startAngle = Math.PI * startValue / 50
        const angleDiff = Math.PI * progressPercent / 50 - startAngle;
        const startAngleDeg = startAngle / Math.PI * 180
        const angleDiffDeg = angleDiff / Math.PI * 180

        mainArc.endAngle(startAngle + angleDiff);
        outerArc.endAngle(startAngle + angleDiff);
      }
    }
  }

  render() {
    return <svg ref={node => this.node = node}></svg>
  }
}

export default DoubleCircleChart;
