import React, {Component} from 'react';
import { scaleLinear } from "d3-scale";
import { select } from "d3-selection";

export default class ProgressBar extends Component {

  constructor(props) {
    super(props);
    this.createProgressBar = this.createProgressBar.bind(this);
  }

  componentDidMount() {
    this.createProgressBar();
  }

  createProgressBar() {
    const node = this.node;

    const svg = select(this.node)
      .attr('width', this.props.size[0])
      .attr('height', 30)

    const xScale = scaleLinear()
      .domain([0, this.props.max])
      .range([0, this.props.size[0]])
      .nice();

    svg.append('rect')
      .attr('class', 'progressbar')
      .attr('rx', 5)
      .attr('ry', 5)
      .attr('fill', 'var(--secondary)')
      .attr('height', 10)
      .attr('width', this.props.size[0])
      .attr('x', 0)

    svg.append('rect')
      .attr('class', 'progressbar')
      .attr('rx', 5)
      .attr('ry', 5)
      .attr('fill', this.props.primaryColor)
      .attr('height', 10)
      .attr('width', xScale(this.props.value))
      .attr('x', 0)
  }

  render() {
    return (
      <svg ref={node => this.node = node}>
      </svg>
    )
  }
}
