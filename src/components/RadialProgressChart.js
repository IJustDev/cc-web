import React, { Component } from "react";
import { scaleLinear } from "d3-scale";
import { max } from "d3-array";
import { select } from "d3-selection";
import { axisLeft } from "d3-axis";
import { arc } from "d3-shape";

import './RadialProgressChart.css';

// https://codepen.io/dabrorius/pen/dWmOgB

class RadialProgressChart extends Component {

  constructor(props){
    super(props)
    this.createProgressChart = this.createProgressChart.bind(this);
  }

  componentDidMount() {
    this.createProgressChart();
  }

  componentDidUpdate() {
    this.createProgressChart();
  }

  createProgressChart() {
    const node = this.node;
    const size = {
      width: this.props.size[0],
      height: this.props.size[1],
    };
    const svg = select(this.node)
      .attr('width', size.width)
      .attr('height', size.height)

    const mainArcGen = arc()
      .startAngle(0)
      .endAngle(Math.PI * 2)
      .innerRadius(size.width / 4)
      .outerRadius(size.width / 4 + 5)

    svg.append('path')
      .attr('class', 'background')
      .attr('fill', '#ccc')
      .attr('fill-opacity', 0.5)
      .attr('d', mainArcGen)
      .attr("transform", `translate(${size.width/2},${size.height/2})`)

    const percentageFilled = this.props.percentage / 100;

    const overlayArcGen = arc()
      .startAngle(Math.PI / 2)
      .endAngle(-percentageFilled * Math.PI * 2 + Math.PI / 2)
      .innerRadius(size.width / 4 - 2)
      .outerRadius(size.width / 4 + 7)
      .cornerRadius(10)

    svg.append('path')
      .attr('fill', this.props.primaryColor)
      .attr('class', 'background')
      .attr('fill-opacity', 0.9)
      .attr('d', overlayArcGen)
      .attr("transform", `translate(${size.width/2},${size.height/2})`)

    svg.append('text')
      .attr('x', size.width/2)
      .attr('y', size.height/2)
      .attr('text-anchor', 'middle')
      .attr('fill', this.props.primaryColor)
      .attr('font-weight', 'bold')
      .attr('dominant-baseline', 'central')
      .text(this.props.percentage + ' %')
  }

  render() {
    return <svg ref={node => this.node = node}></svg>
  }
}

export default RadialProgressChart;
