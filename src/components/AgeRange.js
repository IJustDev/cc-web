import React from 'react';
import './AgeRange.css';

import {numberWithCommas} from '../utils/numbers';

function sumUp(values) {
  let sum = 0;
  for (let value of values) {
    sum += value.value;
  }
  return sum;
}

export default function AgeRange({values}) {
  const sum = sumUp(values);
  return (
    <div className="age-range">
      <ul>
        {values.map((value) => {
          return (
            <li>
              <div className="left low-opacity">
                {value.age}
              </div>
              <div className="right">
                <div className="highlighted">
                  {numberWithCommas(value.value)}
                </div>
                <div class="low-opacity smaller">
                  ({Math.round(parseFloat(value.value / sum) * 100, 4)}%)
                </div>
              </div>
            </li>
          )
        })}
      </ul>
    </div>
  )
}
