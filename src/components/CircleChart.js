import React, {Component} from 'react';
import {select} from 'd3-selection';
import {arc, pie} from 'd3-shape';

class CircleChart extends Component {

  constructor(props) {
    super(props);
    this.createCircleChart = this.createCircleChart.bind(this);
  }

  componentDidMount() {
    this.createCircleChart();
  }

  createCircleChart() {
    const size = {
      width: this.props.size[0],
      height: this.props.size[1],
    };
    const node = this.node;
    const data = this.props.data;
    const svg = select(this.node)
      .attr('width', size.width)
      .attr('height', size.height);

    const arcGen = arc()
      .startAngle(0)
      .endAngle(Math.PI * 2)
      .innerRadius(size.width / 4)
      .outerRadius(size.width / 4 + 15);

    const layout = pie()

    svg.append('path')
      .attr('fill', '#ccc')
      .attr('fill-opactiy', 1)
      .attr('d', arcGen)
      .attr('transform', `translate(${size.width/2}, ${size.height/2})`);

    const partArc = arc()
      .outerRadius(size.width / 4 + 15)
      .innerRadius(size.width / 4);

    svg.selectAll('.arc')
      .data(pie()(data))
      .enter()
      .append('path')
      .attr('class', 'arc')
      .attr('d', partArc)
      .attr('transform', `translate(${size.width/2}, ${size.height/2})`)
      .attr('fill', (data, i) => {
        return this.props.colors[i];
      })

  }

  render() {
    return (
      <svg ref={node => this.node = node}></svg>
    );
  }
}

export default CircleChart;
