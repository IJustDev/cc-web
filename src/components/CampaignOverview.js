import React, { Component } from 'react';


import './CampaignOverview.css';


class CampaignOverview extends Component {

    createPopularity() {
        const popularityArray = [];
        const data = this.props.data;

        for (const element of data) {

            popularityArray.push();
        }
    }

    render() {
        const data = this.props.data;
        return (
            <div className="campaign-overview" >
                {data.map((element) => {
                    return (
                        <div className="campaign-overview-element">
                            <hr />
                            <h3>{element.eventName}</h3>
                            <div className="summary">{element.got}
                                <div className={element.state}>({element.value})</div>
                            </div>
                        </div>
                    )
                })}
            </div>
        )
    }
}

export default CampaignOverview;