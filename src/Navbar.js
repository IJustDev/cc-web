import React from 'react';
import './Navbar.scss';
import logo from './static/img/comeclosely_logo.png';
import closeIcon from './static/img/icons/closeMenu.svg';
import appStroke from './static/img/icons/appStroke.svg';
import arrowDown from './static/img/icons/arrowDown.svg';
import arrowUp from './static/img/icons/arrowUp.svg';
import reports from './static/img/icons/reports.svg';
import fingerUp from './static/img/icons/fingerUp.svg';
import more from './static/img/icons/more.svg';
import avatar from './static/img/icons/avatar.svg';
import notification from './static/img/icons/notification.svg';
import search from './static/img/icons/search.svg';
import dashboard from './static/img/icons/dashboard.svg';

import SalesMonitoring from './pages/SalesMonitoring';
import AppProfile from './pages/AppProfile';
import AdCampaign from './pages/AdCampaign';
import EventManagement from './pages/EventManagement';
import FinanceMonitoring from './pages/FinanceMonitoring';
import HelpdeskManagement from './pages/HelpdeskManagement';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function Navbar(props) {
  const pageTitle = "Helpdesk Management";
  return (
    <div className="Navbar">
      <div className="header">
        <div className="menu__left">
          <div className="menu__left-logo">
            <img src={logo} />
          </div>
          <div className="menu__left-close">
            <a href="javascript:void(0)"><img src={closeIcon} /></a>
          </div>
        </div>
        <div className="menu__middle">
          <div className="menu_middle-app">
            <img src={appStroke} />
            <h2>App</h2>
          </div>
          <ul>
            <li className="no-padding"><h3 className="menu__middle-active">New</h3><img src={arrowDown} /></li>
            <li><img className="menu__middle-icon" src={reports} /><h3>Reports</h3><img src={arrowDown} /></li>
            <li><img className="menu__middle-icon" src={fingerUp} /><h3>Actions</h3><img src={arrowDown} /></li>
          </ul>
        </div>
        <div className="menu__right">
          <ul>
            <li><img src={search} /></li>
            <li><img className="menu__right-notification" src={notification} /></li>
            <li><img className="menu__right-avatar" src={avatar} /></li>
            <li><img src={more} /></li>
          </ul>
        </div>
      </div>
      <Router>
        <div className="wrapper">
          <div className="sidebar">
            <ul>
              <li className="sidebar__title">
                <img src={dashboard} className="sidebar__title-dashboard" />
                <h2>Dashboard</h2>
                <span>4</span>
                <img src={arrowUp} />
              </li>
              <li className="sidebar__active"><Link to="/app-profile">App Profile</Link></li>
              <li><Link to="/sales-monitoring">Sales Monitoring</Link></li>
              <li><Link to="/ad-campaign">Ad Campaign</Link></li>
              <li><Link to="/event-management">Event Management</Link></li>
              <li><Link to="/finance-monitoring">Finance Monitoring</Link></li>
              <li><Link to="/helpdesk-management">Helpdesk Management</Link></li>
            </ul>
          </div>
          <div className="wrapper__content horizontal">
            <div className="breadcrumbs">
              <div className="breadcrumb-title">
                {pageTitle}
              </div>
              <div className="breadcrumb-link">
                Home > Dashboard > <div className="breadcrumb-link__active"> {pageTitle}</div>
              </div>
            </div>
            <div className="content">
              <Switch>
                <Route path="/app-profile">
                  <AppProfile />
                </Route>
                <Route path="/sales-monitoring">
                  <SalesMonitoring />
                </Route>
                <Route path="/ad-campaign">
                  <AdCampaign />
                </Route>
                <Route path="/event-management">
                  <EventManagement />
                </Route>
                <Route path="/finance-monitoring">
                  <FinanceMonitoring />
                </Route>
                <Route path="/helpdesk-management">
                  <HelpdeskManagement />
                </Route>
              </Switch>
            </div>
          </div>
        </div>
      </Router>
    </div >
  );
}

export default Navbar;
