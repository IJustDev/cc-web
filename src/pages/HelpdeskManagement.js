import React from 'react';
import Card from '../Card';
import IconCard from '../IconCard';
import './HelpdeskManagement.css';
import MessagesCard from '../components/MessagesCard';
import BarChart from '../components/BarChart';
import RadialProgressChart from '../components/RadialProgressChart';
import DoubleCircleChart from '../components/DoubleCircleChart';
import LineChart from '../components/LineChart';
import List from '../components/List';
import ListItem from '../components/ListItem';
import ContactList from '../components/ContactList';
import ProgressBar from '../components/ProgressBar';

function HelpdeskManagement() {
  return (
    <div className="HelpdeskManagement">
      <div className="row">
        <Card title="Support Requests">
          <BarChart data={[20, 30, 50, 20, 150, 20]}
            primaryColor="#ff00a7"
            size={[200,200]}/>
        </Card>
        <Card title="Complaints">
          <BarChart data={[20, 30, 50, 20, 150, 20]}
            primaryColor="#ffcc41"
            size={[200,200]}/>
        </Card>
        <IconCard title="Time To Resolve Complaint" iconUrl="https://image.flaticon.com/icons/png/512/29/29495.png">
          <div class="centered">
            <ProgressBar
              text="Female"
              value={80}
              max={95}
              primaryColor="#b620e0"
              size={[200,15]}
            />
            <h2>58m:50s</h2>
            <h3>(Goal: 8m:0s)</h3>
          </div>
        </IconCard>
        <IconCard title="Average Speed of Answer">
          <div class="centered">
            <h3>Measure how quickly support staff answer incoming calls.</h3>
            <h2 class="highlighted p5">24m:20s</h2>
          </div>
        </IconCard>
      </div>
      <div className="row">
        <Card title="Customer Satisfaction">
          <DoubleCircleChart size={[250, 150]}/>
        </Card>
        <Card title="Top Performer Help Agents">
          <ContactList/>
        </Card>
        <MessagesCard/>
      </div>
      <div className="row">
      </div>
    </div>
  );
}

export default HelpdeskManagement;
