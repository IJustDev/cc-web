import React from 'react';
import Card from '../Card';
import IconCard from '../IconCard';
import '../App.css';
import MessagesCard from '../components/MessagesCard';
import BarChart from '../components/BarChart';
import RadialProgressChart from '../components/RadialProgressChart';
import DoubleCircleChart from '../components/DoubleCircleChart';
import LineChart from '../components/LineChart';
import List from '../components/List';
import ListItem from '../components/ListItem';
import ContactList from '../components/ContactList';
import AgeRange from '../components/AgeRange';
import LegendProgressBar from '../components/LegendProgressBar';
import CircleChart from '../components/CircleChart';


function AppProfile() {
  return (
    <div className="AdCampaign">
      <div className="row">
        <Card title="Support Requests">
          <BarChart data={[20, 30, 50, 20, 150, 20]}
            primaryColor="#b620e0"
            size={[200,200]}/>
        </Card>
        <Card title="Complaints">
          <LineChart data={[
            {y: 0, x: 10},
            {y: 68, x: 40},
            {y: 0, x: 55},
            {y: 55, x: 80}
          ]}
          smoothed={true}
          size={[200, 200]}/>
        </Card>
        <IconCard title="Time To Resolve Complaint" iconUrl="https://image.flaticon.com/icons/png/512/29/29495.png">
          <RadialProgressChart
            value={20}
            primaryColor="var(--secondary-shade)"
            percentage={30}
            size={[150, 150]}/>
        </IconCard>
        <IconCard title="Average Speed of Answer">
          <RadialProgressChart
            value={80}
            size={[150, 150]}
            percentage={75}
            primaryColor='var(--primary)'
          />
        </IconCard>
      </div>
      <div className="row">
        <MessagesCard/>
        <Card title="Customer Satisfaction">
          <CircleChart
            size={[200, 200]}
            data={[15, 80]}
            colors={['skyblue', 'darkred']}
          />
          <LegendProgressBar
            text="Male"
            value={15}
            max={95}
            primaryColor="skyblue"
          />
          <LegendProgressBar
            text="Female"
            value={80}
            max={95}
            primaryColor="darkred"
          />
        </Card>
        <Card title="Age Range">
          <AgeRange values={[
            {
              age: '17>',
              value: 7800,
            },
            {
              age: '18-24',
              value: 12010,
            },
            {
              age: '24-30',
              value: 9974,
            },
            {
              age: '30-36',
              value: 8390,
            },
            {
              age: '36+',
              value: 6740,
            }
          ]}/>
      </Card>
    </div>
    <div className="row">
    </div>
  </div>
  );
}

export default AppProfile;
