import React from 'react';
import Card from '../Card';
import IconCard from '../IconCard';
import './EventManagement.css';
import UpcomingEvents from '../components/UpcomingEvents';
import LegendProgressBar from '../components/LegendProgressBar';

function EventManagement() {
  return (
    <div className="EventManagement">
      <div className="row">
        <Card title="Upcoming Events">
          <UpcomingEvents
            events={[
              {
                title: 'PH World Mall Lantern Festival',
                date: 'Jan 22 Tuesday',
                time: '8 AM - 4 PM',
                status: 0,
              },
              {
                title: 'PH World Mall Lantern Festival',
                date: 'Jan 22 Tuesday',
                time: '8 AM - 4 PM',
                status: 1,
              },
              {
                title: 'PH World Mall Lantern Festival',
                date: 'Jan 22 Tuesday',
                time: '8 AM - 4 PM',
                status: 2,
              },
              {
                title: 'PH World Mall Lantern Festival',
                date: 'Jan 22 Tuesday',
                time: '8 AM - 4 PM',
                status: 3,
              },
            ]}
          />
        </Card>
        <Card title="Goals">
          <div className="row">
            <LegendProgressBar
              text="Events Hosted"
              value={1350}
              max={1500}
              className="m5"
              primaryColor="#42e0fc"
            />
            <LegendProgressBar
              text="Revenue"
              value={8896000}
              max={50000000}
              primaryColor="#9013fe"
            />
          </div>
          <div className="row">
            <LegendProgressBar
              text="Events That Earned"
              value={1500}
              max={1800}
              primaryColor="#ff8f00"
            />
            <LegendProgressBar
              text="Ticket Sold"
              value={1350}
              max={1500}
              primaryColor="#b8e986"
            />
          </div>
        </Card>
      </div>
    </div>
  );
}

export default EventManagement;
