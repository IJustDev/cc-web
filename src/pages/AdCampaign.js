import React from 'react';
import Card from '../Card';
import IconCard from '../IconCard';
import '../App.css';
import TableZone from '../components/TableZone';
import CampaignOverview from '../components/CampaignOverview';
import BarChart from '../components/BarChart';
import RadialProgressChart from '../components/RadialProgressChart';
import DoubleCircleChart from '../components/DoubleCircleChart';
import LineChart from '../components/LineChart';
import List from '../components/List';
import ListItem from '../components/ListItem';
import ContactList from '../components/ContactList';

function AdCampaign() {
  return (
    <div className="AdCampaign">
      <div className="row">
        <Card title="Support Requests">
          <BarChart data={[20, 30, 50, 20, 150, 20]}
            primaryColor="#ff0050"
            size={[200, 200]} />
        </Card>
        <Card title="Complaints">
          <LineChart data={[
            { y: 0, x: 10 },
            { y: 13, x: 15 },
            { y: 24, x: 20 },
            { y: 35, x: 25 },
            { y: 46, x: 30 },
            { y: 57, x: 35 },
            { y: 68, x: 40 },
            { y: 79, x: 45 },
            { y: 69, x: 50 },
            { y: 0, x: 55 },
            { y: 11, x: 60 },
            { y: 22, x: 65 },
            { y: 33, x: 70 },
            { y: 44, x: 75 },
            { y: 55, x: 80 }
          ]}
            smoothed={true}
            size={[200, 200]} />
        </Card>
        <IconCard title="Time To Resolve Complaint" iconUrl="https://image.flaticon.com/icons/png/512/29/29495.png">
          <RadialProgressChart
            value={20}
            size={[250, 100]} />
        </IconCard>
        <Card title="Popularity">
          <CampaignOverview data={[
            { eventName: 'Global Event Rank', got: "1,137", state: "good", value: "+150" },
            { eventName: 'Global Event Rank', got: "1,137", state: "middle", value: "-15" },
          ]} />
        </Card>
      </div>
      <div className="row">
        <Card title="Featured Zone">
          <TableZone data={[
            { action: 'Click', value: '4,275' },
            { action: 'View', value: '12,773' },
            { action: 'Sales', value: 639 },
          ]} />
        </Card>
        <Card title="Customer Satisfaction">
          <DoubleCircleChart size={[250, 150]} />
        </Card>
        <Card title="Spotlight Zone">
          <TableZone data={[
            { action: 'Click', value: '17,250' },
            { action: 'View', value: '67,320' },
            { action: 'Sales', value: '2,382' },
          ]} />
        </Card>
      </div>
      <div className="row">
      </div>
    </div>
  );
}

export default AdCampaign;
