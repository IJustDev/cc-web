import React from 'react';
import './Card.css';

function Card(props) {
  return (
    <div className="card">
      <h1 className="card-title">{props.title}</h1>
      {props.renderLine && <hr/>}
      <div className="card-content">
        {props.children}
      </div>
    </div>
  );
}

export default Card;
